# Deliverables

### Este repositorio contiene los entregables del proyecto integrador.

1. Entregable 1 (8 - 12 de Marzo):

- Informe:

  - Nombre de los estudiantes.
  - Semestre.
  - Grupo.
  - Asigntaruas que integran el proyecto.
  - Aporte de cada asignatura.
  - Producto final.
  - Cronograma.

2. Entregable 2 (12 - 17 de Abril):

- 40% - 50% del proyecto.


3. Entregable 3 (10 - 15 de Mayo):

- 90% - 100% del proyecto.


4. Entregable 4 (17 - 22 de Mayo):

- Informe técnico.

5. Entregable 5 (24 - 29 de Mayo):

- Video demostrativo del proyecto.
